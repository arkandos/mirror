# WebCam Mirror

A **extremly** simple page, showing you your own webcam as a mirror. Like, literally 3 lines of code.

I was tired of searching "webcam test online", clicking on a random result, and then maybe sending my stream to some place on the internet? How can know?

So I made my own site where I can be sure.

